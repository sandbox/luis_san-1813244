
jQuery(document).ready(	function(){
	btkOnLoad_2();
		$.ajaxSetup({
		complete: function () {
			    btkOnLoad_2();
		}
	});	
});


function btkOnLoad_2() {
	if ($('.payment-link-premium').size()) {
		$('.payment-link-premium').bt({
			trigger: 'click',
			ajaxPath: '../payments_methods_premium #-payments-methods-premium',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
		$('.payment-link-premium').bt({
			trigger: 'mouseover',
			ajaxPath: '../payments_methods_premium #-payments-methods-premium',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
	}
	if ($('.payment-link-leading').size()) {
		$('.payment-link-leading').bt({
			trigger: 'click',
			ajaxPath: '../payments_methods_leading #-payments-methods-leading',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
		$('.payment-link-leading').bt({
			trigger: 'mouseover',
			ajaxPath: '../payments_methods_leading #-payments-methods-leading',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
	}
	if ($('.payment-link-banner').size()) {
		$('.payment-link-banner').bt({
			trigger: 'click',
			ajaxPath: '../payments_methods_banner #-payments-methods-banner',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
		$('.payment-link-banner').bt({
			trigger: 'mouseover',
			ajaxPath: '../payments_methods_banner #-payments-methods-banner',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
	}
	if ($('.payment-link-standard').size()) {
		$('.payment-link-standard').bt({
			trigger: 'click',
			ajaxPath: '../payments_methods_standard #-payments-methods-standard',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
		$('.payment-link-standard').bt({
			trigger: 'mouseover',
			ajaxPath: '../payments_methods_standard #-payments-methods-standard',
			width: 250, 
			fill: 'white', 
			cssStyles: {color: 'black'},
			cornerRadius: 20, 
			padding: 20,
			closeWhenOthersOpen : true,
		});
	}
}
