
Configuraci�n del m�dulo

Requiere tener instalado previamente dentro de "sites/all/libraries" la carpeta "bt" (BeautyTips 
"http://www.lullabot.com/files/bt/bt-latest/DEMO/index.html").

Requiere tener instalado y activo previamente el m�dulo "subscriptions_expire"

1- Instalar el m�dulo en la carpeta site/all/modules y activar.
2- Para crear un link que muestre un bubble con las pasarelas de pago hay que incluirlo dentro de un anchor y 
con la clase "payment-link" de esta forma: 
Standard o Normal: "<p>Click <div id="payment-link-standard" class="payment-link-standard">here</div> to pay.</p>"
Premium: "<p>Click <div id="payment-link-premium" class="payment-link-premium">here</div> to pay.</p>"
Leading: "<p>Click <div id="payment-link-leading" class="payment-link-leading">here</div> to pay.</p>"
Banner: "<p>Click <div id="payment-link-banner" class="payment-link-banner">here</div> to pay.</p>"
3- Administrar por el men� "Site configuration"->"Subscriptions and expire settings" o por el path 
  "admin/settings/subscriptions_expire".
  3.1- Setear en el apartado "Lead and Premium Escorts" los valores para los tipos de suscripci�n.
