                =============================================
                =                R E A D M E                =
                =============================================


PREREQUISITES:

  We assume you already have your merchant ID (MID) and a test certificate
  (available from techsupport@paysafecard.com)

INSTRUCTIONS:

  Copy the certificate and the merchant_direct.properties file
  to an unaccessable directory on your server
  
  configure merchant_direct.properties
  customize paths and URLs in configure.php
  
  see /doc/PHP-API-libcurl_en.html for more details